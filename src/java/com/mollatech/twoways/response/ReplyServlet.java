/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.twoways.response;

import com.mollatech.axiom.nucleus.db.Txdetails;
import com.mollatech.axiom.nucleus.db.connector.management.TxManagement;
import com.twilio.sdk.verbs.Reject;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author WALE
 */
public class ReplyServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // twilio 
//        String from = "" + request.getParameter("From");
//        String smsid = request.getParameter("SmsSid");
//        String accountSid = request.getParameter("AccountSid");
        String to = request.getParameter("To");
//        String body = request.getParameter("Body");
//        String fromCity = request.getParameter("FromCity");
//        String fromState = request.getParameter("FromState");
//        String fromZip = request.getParameter("FromZip");
//        String FromCountry = request.getParameter("FromCountry");
//        String toCity = request.getParameter("ToCity");
//        String toState = request.getParameter("ToState");
//        String toZip = request.getParameter("ToZip");
//        String toCountry = request.getParameter("ToCountry");
//        String msg = request.getParameter("Message[0]");
        String digits = request.getParameter("Digits");
//        String userid = request.getParameter("userid");
//        String txid = request.getParameter("txid");
//        String resmessage = request.getParameter("response");
//        String responsetype = request.getParameter("type");

        System.out.println(digits);
        TwiMLResponse twiml = new TwiMLResponse();
        Txdetails txd = null;
        Say say = new Say("Thank you");
        Say say1 = new Say("You have entered " + digits);
        if (to != null) {
            txd = new TxManagement().getTxDetailsByPhone(to);
            System.out.println(digits);
            try {
                txd.setResponse(digits);
                System.out.println(digits);
                txd.setStatus(Txdetails.RESPONDED);
            } catch (Exception e) {
                e.printStackTrace();
                txd.setStatus(Txdetails.PENDING);
            }

        }
        Reject r = new Reject();
        String rej = "ended";
        r.setReason(rej);
        try {
            twiml.append(say1);
            twiml.append(say);
        } catch (Exception e) {
            e.printStackTrace();
        }

        response.setContentType("application/xml");
        response.getWriter().print(twiml.toXML());
        System.out.println("Reply servlet" + twiml.toXML());
        txd.setUpdatedOn(new Date());
        int res = new TxManagement().editTxDetails(txd);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
