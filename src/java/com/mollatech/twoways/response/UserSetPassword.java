/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.twoways.response;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
//@WebServlet(name = "UserSetPassword", urlPatterns = {"/UserSetPassword"})
public class UserSetPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String callback = request.getParameter("callback");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/json");
        String userid = request.getParameter("userid");
        String password = request.getParameter("password");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        String channelName = LoadSettings.g_sSettings.getProperty("axiom.face.template");
        if (channelName == null) {
            channelName = "face";
        }
        channelName = channelName.replace(".war", "");
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(channelName);
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        SessionManagement sManagement = new SessionManagement();
        try {

            if (credentialInfo == null) {
                String result = "error";
                String message = "Remote Access is not configured properly!!";

                json.put("_result", result);
                json.put("_message", message);

            }
            String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
            AuthUser user = new UserManagement().getUser(sessionId, channel.getChannelid(), userid);
            if (user == null) {
                String result = "error";
                String message = "User Not Found";
                json.put("_result", result);
                json.put("_message", message);
            } else {
                String pwd = new UserManagement().GetPassword(sessionId, channel.getChannelid(), userid);
                int res = 0;
//                        new UserManagement().changeNullPassword(sessionId, channel.getChannelid(), userid, pwd, password);
                if (res == 0) {
                    String result = "success";
                    String message = "success";
                    json.put("userid", user.userId);
                    json.put("_result", result);
                    json.put("_message", message);
                } else {
                    String result = "error";
                    String message = "Error in Changing Password ";
                    json.put("_result", result);
                    json.put("_message", message);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            out.print(callback + "(" + json + ")");
            out.flush();
            sRemoteAcess.close();
            suRemoteAcess.close();
            sChannel.close();
            suChannel.close();
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
