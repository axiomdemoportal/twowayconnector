/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.twoways.response;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Registerdevicepush;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
//import static com.mollatech.twoway.response.registerpushdevice.ACTIVE;
//import static com.mollatech.twoway.response.registerpushdevice.ANDROID;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
//@WebServlet(name = "iospushregisterdevice", urlPatterns = {"/iospushregisterdevice"})
public class iospushregisterdevice extends HttpServlet {

    public static final int USERID = 1;
    public static final int PHONENUMBER = 2;
    public static final int EMAILID = 3;
    public static final int USERNAME = 4;

    public static final int ACTIVE = 1;
    public static final int SUSPENDED = -1;
    public static final int ANDROID = 1;
    public static final int IOS = 2;
    public static final int LENDER_APP = 1;
    public static final int BORROWER_APP = 2;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/json");
        String callback = request.getParameter("callback");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        String channelName = LoadSettings.g_sSettings.getProperty("axiom.face.template");
         if (channelName == null) {
            channelName = "face";
        }
        channelName = channelName.replace(".war", "");
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(channelName);
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        SessionManagement sManagement = new SessionManagement();
        String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        try {

            if (credentialInfo == null) {
                String result = "error";
                String message = "Remote Access is not configured properly!!";

                json.put("_result", result);
                json.put("_message", message);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {

            String regCode = request.getParameter("_regid");
            String type = request.getParameter("apptype");
            String id = request.getParameter("id");
            int apptype = 1;

            if (type != null) {
                apptype = Integer.parseInt(type);
            }

            int iType = -1;
            String uId = "";
            String emailid = null;
            // 1 for lender

            iType = 1;
            UserManagement management = new UserManagement();
            String passcheck = "no";
            AuthUser lenders = management.getUser(sessionId, channel.getChannelid(), id);
           
            
            if (lenders == null) {
                try {
                    json.put("_result", "error");
                    json.put("_message", "User not found");
//                        out.print(callback + "(" + json + ")");
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            uId = lenders.userId;
            emailid = lenders.getEmail();

            //2 for borrower
            ////   String email = request.getParameter("email");
            String gcm_regid = request.getParameter("_regid");
            String error = "Failed to register!!";
            int errorcode = -1;
            json.put("_result", error);
            json.put("_message", errorcode);

            /**
             * Registering a user device Store reg id in users table
             *
             */
            
                if (gcm_regid == null || regCode == null) {
                    error = "Invalid parameters!!!";
                    errorcode = -2;
                    json.put("_result", error);
                    json.put("_message", errorcode);
//                    out.print(callback + "(" + json + ")");
//                out.print(json);
//                out.flush();
                    return;

                }
            String _channelName = this.getServletContext().getContextPath();
            _channelName = _channelName.replaceAll("/", "");

            UserManagement uManagement = new UserManagement();

            PushNotificationDeviceManagement pManagement = new PushNotificationDeviceManagement();

            int result = -1;
            Registerdevicepush registerdevicepush = pManagement.GetRegisterPushDeviceByEmailIDAndType(sessionId, channel.getChannelid(), emailid, IOS);

            if (registerdevicepush != null) {
                if (!gcm_regid.equals(registerdevicepush.getGoogleregisterid())) {
                    registerdevicepush.setGoogleregisterid(gcm_regid);
                    registerdevicepush.setUserid(id);
                    registerdevicepush.setType(IOS);
                    int res = pManagement.ChangeRegisterPushDevice(sessionId, channel.getChannelid(), uId, registerdevicepush);
                    if (res == 0) {
                        error = "success";
                        errorcode = 0;
                        try {
                            json.put("_result", "success");
                            json.put("_message", "Device registered successfully...");
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } else {
                    try {
                        json.put("_result", "success");
                        json.put("_message", "Device registered successfully...");
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
            result = pManagement.AddRegisterPushDevice(sessionId, channel.getChannelid(), ACTIVE, emailid, gcm_regid, uId, IOS, new Date(), new Date());
        //(ACTIVE, emailid, gcm_regid, String.valueOf(uId), IOS, new Date(), new Date());
            if (result == 0) {
                error = "success";
                errorcode = 0;
                try {
                    json.put("_result", "success");
                    json.put("_message", "Device registered successfully...");
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.print(callback + "(" + json + ")");
            out.flush();
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
