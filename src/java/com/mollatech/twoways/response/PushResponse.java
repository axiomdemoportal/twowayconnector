/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.twoways.response;

import com.mollatech.axiom.nucleus.db.Txdetails;
import com.mollatech.axiom.nucleus.db.connector.management.TxManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class PushResponse extends HttpServlet {

   

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userid = request.getParameter("userid");
        String callback = request.getParameter("callback");
        response.setHeader("Access-Control-Allow-Origin", "*");
        String txid = request.getParameter("txid");
        String responsetype = request.getParameter("type");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        Date time = new Date();
        try {
            Txdetails txd = new TxManagement().getTxDetailsByTxId(txid);
            if (time.before(txd.getExpiredOn()) && (txd.getStatus() != Txdetails.RESPONDED && txd.getStatus() != Txdetails.APPROVED && txd.getStatus() != Txdetails.DENIED)) {
                int iResponsetype = Integer.parseInt(responsetype);
                if (iResponsetype == 1) {
                    txd.setStatus(Txdetails.APPROVED);
                } else {
                    txd.setStatus(Txdetails.DENIED);
                }
                txd.setUpdatedOn(new Date());
                int res = new TxManagement().editTxDetails(txd);
                if (res == 0) {
                    String question = "";
                    if (txd.getQuestion() != null && txd.getStatus() == Txdetails.APPROVED) {
                        if (!txd.getQuestion().trim().equals("")) {
                            question = txd.getQuestion();
                        }
                    }
                    String result = "success";
                    String _message = "Your Action Is Updated";
                    json.put("_result", result);
                    json.put("_question", question);
                    json.put("_message", _message);
                    return;
                } else {
                    String result = "error";
                    String _message = "Transaction Session Expired";
                    json.put("_result", result);
                    json.put("_message", _message);
                    return;
                }

            } else {
                String result = "error";
                String _message = "error";
                if (txd.getStatus() != Txdetails.RESPONDED || txd.getStatus() != Txdetails.APPROVED || txd.getStatus() != Txdetails.DENIED) {
                    _message = "Already Responded";
                } else {
                    _message = "Transaction Session Expired";
                    txd.setUpdatedOn(new Date());
                    txd.setStatus(Txdetails.EXPIRED);
                    int res = new TxManagement().editTxDetails(txd);
                }

                json.put("_result", result);
                json.put("_message", _message);
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.print(callback + "(" + json + ")");
            out.flush();
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
