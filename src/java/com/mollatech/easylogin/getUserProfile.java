/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.easylogin;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.TokenStatusDetails;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Certificates;
import com.mollatech.axiom.nucleus.db.Usergroups;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GroupManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author parimal
 */
public class getUserProfile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        String callback = request.getParameter("callback");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String channelId = LoadSettings.g_sSettings.getProperty("axiom.esigner.channelId");
        String userId = request.getParameter("userId");
        String _certstatus = "Suspended";
        if (userId != null) {
            TokenSettings tokenObj = new TokenSettings();
            SessionManagement sm = new SessionManagement();
            String sessionid = sm.OpenSession(channelId, "dc57912c214", "dc57912c214", request.getSession().getId());
            SettingsManagement sMngmt = new SettingsManagement();
            UserManagement umngt = new UserManagement();
            AuthUser Users = null;
            String otpduration = null;
            String otplength = null;
            String otpsrno = null;
            Users = umngt.getUser(channelId, userId);
            if (Users != null) {
                Object settingsObj = sMngmt.getSetting(sessionid, channelId, 7, 1);
                if (settingsObj != null) {
                    tokenObj = (TokenSettings) settingsObj;
                    otpduration = Integer.toString(tokenObj.getOtpDurationInSeconds());
                    otplength = Integer.toString(tokenObj.getOtpLengthSWToken());
                }
                OTPTokenManagement otpmngt = new OTPTokenManagement(channelId);
                TokenStatusDetails t = otpmngt.getToken(sessionid, channelId, userId, 1);
                otpsrno = t.serialnumber;
                //get certificate details
                javax.security.cert.X509Certificate certifts = null;
                SettingsManagement smngt = new SettingsManagement();
                RootCertificateSettings rCertSettings = (RootCertificateSettings) smngt.getSetting(channelId, SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_ONE);
                rCertSettings.getKeyLength();
                CertificateManagement cmngt = new CertificateManagement();
                int certstatus = cmngt.GetStatus(channelId, userId);
                Certificates cert = cmngt.getCertificate(channelId, userId);
                if (certstatus == 1) {
                    _certstatus = "Active";
                }
                if (certstatus == -5) {
                    _certstatus = "Revoked";
                }
                if (certstatus == -10) {
                    _certstatus = "Expired";
                }
                if (cert != null) {
                    byte[] certBytes = Base64.decode(cert.getCertificate());
                    try {
                        certifts = javax.security.cert.X509Certificate.getInstance(certBytes);
                        if (certifts == null) {
                            JSONObject json = new JSONObject();
                            try {
                                json.put("_result", "error");
                                json.put("_message", "Certificate Decoding Failed!!!");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            out.print(callback + "(" + json + ")");
                            out.flush();
                            out.close();
                        }
                    } catch (Exception ex) {
                        JSONObject json = new JSONObject();
                        try {
                            json.put("_result", "error");
                            json.put("_message", "Certificate Decoding Failed!!!");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        out.print(callback + "(" + json + ")");
                        out.flush();
                        out.close();
                    }
                } else {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("_result", "error");
                        json.put("_message", "notissued");
                    } catch (Exception ex) {
                        Logger.getLogger(getUserProfile.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    out.print(callback + "(" + json + ")");
                    out.flush();
                    out.close();
                }
                JSONObject json = new JSONObject();
                if (cert == null) {
                    try {
                        json.put("_result", "error");
                    } catch (Exception ex) {
                        Logger.getLogger(getUserProfile.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    out.print(callback + "(" + json + ")");
                    out.flush();
                    out.close();
                }
                try {
                    String certemail = null;
                    String certcn = null;
                    json.put("certstatus", _certstatus);

                    String issuer = certifts.getIssuerDN().toString();
                    String[] subject = certifts.getSubjectDN().getName().split(",");
                    for (String s : subject) {
                        if (s.contains("EMAILADDRESS")) {
                            certemail = s.split("=")[1];
                        }
                    }
                    String[] _issuer = issuer.split(",");
                    for (int i = 0; i < _issuer.length; i++) {
                        if (_issuer[i].toLowerCase().contains("cn=")) {
                            certcn = _issuer[i].split("=")[1];
                        }
                    }
                    json.put("otpsrno", otpsrno);
                    json.put("otplength", otplength);
                    json.put("otpduration", otpduration);
                    json.put("srno", certifts.getSerialNumber());
                    json.put("algoname", certifts.getSigAlgName());
                    json.put("issuerdn", "\"" + certifts.getIssuerDN() + "\"");
                    json.put("certemail", certemail);
                    json.put("certcn", certcn);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String tilldate = sdf.format(certifts.getNotAfter());
                    String fromdate = sdf.format(certifts.getNotBefore());
                    json.put("notafter", tilldate);
                    json.put("notbefore", fromdate);
                    json.put("version", certifts.getVersion());
                    json.put("subjectdn", "\"" + certifts.getSubjectDN() + "\"");
                    json.put("result", "success");
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        json.put("_result", "error");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    out.print(callback + "(" + json + ")");
                    out.flush();
                    out.close();
                }
                out.print(callback + "(" + json + ")");
                out.flush();
                out.close();
            } else {
                JSONObject json = new JSONObject();
                try {
                    json.put("_result", "error");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                out.print(callback + "(" + json + ")");
                out.flush();
                out.close();
            }
        } else {
            JSONObject json = new JSONObject();
            try {
                json.put("_result", "error");
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(callback + "(" + json + ")");
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
