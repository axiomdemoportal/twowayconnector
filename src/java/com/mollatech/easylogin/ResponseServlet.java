package com.mollatech.easylogin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mollatech.axiom.nucleus.db.Txdetails;
import com.mollatech.axiom.nucleus.db.connector.management.TxManagement;
import com.twilio.sdk.verbs.Gather;
import com.twilio.sdk.verbs.Reject;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class ResponseServlet extends HttpServlet {

    public void init() {
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int type = -1;
        String callback = request.getParameter("callback");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();
        // twilio 
        String from = "" + request.getParameter("From");
//        String smsid = request.getParameter("SmsSid");
//        String accountSid = request.getParameter("AccountSid");
        String to = request.getParameter("To");
//        String body = request.getParameter("Body");
//        String fromCity = request.getParameter("FromCity");
//        String fromState = request.getParameter("FromState");
//        String fromZip = request.getParameter("FromZip");
//        String FromCountry = request.getParameter("FromCountry");
//        String toCity = request.getParameter("ToCity");
//        String toState = request.getParameter("ToState");
//        String toZip = request.getParameter("ToZip");
//        String toCountry = request.getParameter("ToCountry");
        String msg = request.getParameter("Message[0]");
        String digits = request.getParameter("Digits");
        String userid = request.getParameter("userid");
        String txid = request.getParameter("txid");
        String resmessage = request.getParameter("response");
        String responsetype = request.getParameter("type");

// karp system
        String message = request.getParameter("message");
        String sender = request.getParameter("from");

        Date time = new Date();
        if (to == null) {
            to = sender;
        }
        Txdetails txd = null;
        if (txid != null) {
            txd = new TxManagement().getTxDetailsByTxId(txid);
        } else if (to != null) {
            txd = new TxManagement().getTxDetailsByPhone(to);
            if (txd == null) {
                to = to.trim();
                String tempto = "+" + to;
                txd = new TxManagement().getTxDetailsByPhone(tempto);
            }
        }
        if (txd == null) {
            from = from.trim();
            String temp = "+" + from;
            txd = new TxManagement().getTxDetailsByPhone(temp);
        }
        if (time.before(txd.getExpiredOn()) && txd.getStatus() != Txdetails.RESPONDED) {
            type = txd.getType();
            if (type == Txdetails.SMS) // for SMS
            {
                try {
                    String mess = message.replaceAll("DICTUM ", "");
                    mess = mess.trim();
                    txd.setResponse(mess);
                    System.out.println(mess);
                    txd.setStatus(Txdetails.RESPONDED);
                } catch (Exception e) {
                    e.printStackTrace();
                    txd.setStatus(Txdetails.PENDING);
                }
            } else if (type == Txdetails.VOICE) //    for IVR
            {
                TwiMLResponse twiml = new TwiMLResponse();
                Say say = new Say(msg);
                Gather gather = new Gather();
                gather.setNumDigits(4);
                System.out.println("response in digit" + digits);
                gather.setAction("http://182.70.115.176:8888/TwoWayConnector/ReplyServlet");
                gather.setMethod("POST");
                try {
                    gather.append(say);
                    twiml.append(gather);
                    say = new Say(digits);
                } catch (Exception e) {
                    e.printStackTrace();
                    txd.setStatus(Txdetails.PENDING);
                }
                response.setContentType("application/xml");
                response.getWriter().print(twiml.toXML());
                System.out.println("Response servlet" + twiml.toXML());
                txd.setResponse(digits);
                System.out.println(digits);
                txd.setStatus(Txdetails.RESPONDED);
            } else if (type == Txdetails.MISSEDCALL) // missed call
            {

                TwiMLResponse twiml = new TwiMLResponse();
                try {
                    Reject r = new Reject();
                    String rej = "";
                    r.setReason(rej);
                    twiml.append(r);
                    response.setContentType("application/xml");
                    response.getWriter().print(twiml.toXML());
                    txd.setResponse("OK");
                    txd.setStatus(Txdetails.RESPONDED);
                } catch (Exception e) {
                    e.printStackTrace();
                    txd.setStatus(Txdetails.PENDING);
                }
            } else if (type == Txdetails.PUSH) // push messages
            {
                JSONObject json = new JSONObject();
                int restype = Integer.parseInt(responsetype);
                try {
                    if (restype == 1) {

                        if (txd.getUserid().equals(userid)) {
                            txd.setResponse(resmessage);
                            txd.setStatus(Txdetails.RESPONDED);
                        } else {
                            String result = "error";
                            String _message = "error";
                            json.put("_error", result);
                            json.put("_error", _message);
                            return;
                        }
                    } else {
                        txd.setStatus(Txdetails.RESPONDED);
                        txd.setResponse("");

                    }
                    txd.setUpdatedOn(new Date());
                    int res = new TxManagement().editTxDetails(txd);
                    if (res == 0) {
                        String result = "success";
                        String _message = "success";
                        json.put("_result", result);
                        json.put("_message", _message);
                    } else {
                        String result = "error";
                        String _message = "error";
                        json.put("_result", result);
                        json.put("_message", _message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    txd.setStatus(Txdetails.PENDING);
                } finally {
                    out.print(callback + "(" + json + ")");
                    out.flush();
                    return;
                }
            }
        } else {
            if (txd.getStatus() != Txdetails.RESPONDED) {
                txd.setStatus(Txdetails.EXPIRED);
            } else {
                try {
                    if (txd.getType() == Txdetails.PUSH) {
                        String result = "error";
                        String _message = "Already Responded";
                        JSONObject json = new JSONObject();
                        json.put("_result", result);
                        json.put("_message", _message);
                        out.print(callback + "(" + json + ")");
                        out.flush();
                        return;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return;
            }
        }
        txd.setUpdatedOn(new Date());
        int res = new TxManagement().editTxDetails(txd);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
