///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mollatech.easylogin;
//
//import com.mollatech.axiom.nucleus.crypto.LoadSettings;
//import com.mollatech.axiom.nucleus.db.ApEasylogin;
//import com.mollatech.axiom.nucleus.db.ApEasyloginsession;
//import com.mollatech.axiom.nucleus.db.Channels;
//import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
//import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
//import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
//import com.mollatech.axiom.nucleus.db.connector.management.EasyLoginManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.EasyLoginSessionManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
//import com.mollatech.axiom.v2.face.operations.reverseGeoCode;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.bouncycastle.util.encoders.Base64;
//import org.hibernate.Session;
//import org.json.JSONObject;
//
///**
// *
// * @author mohanish
// */
//public class AddEasyLogin_old extends HttpServlet {
//
//    public final int EXPIRED_STATUS = -2;
//    public final int SUSPENDED_STATUS = -1;
//    public final int PENDING_STATUS = 0;
//    public static final int ACTIVE_STATUS = 1;
//    public final int COMPLETED_STATUS = 2;
//
//    /**
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
//     * methods.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        int result1 = -1, status = 0;
//        String servermsg = "";
//        String callback = request.getParameter("callback");
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setContentType("application/json");
//        String ip = request.getRemoteAddr();
//        String lattitude = request.getParameter("_lattitude");
//        String longitude = request.getParameter("_longitude");
//        String userid = request.getParameter("_userid");
//        String deviceid = request.getParameter("_deviceid");
//        String deviceinfo = request.getParameter("_deviceinfo");
//        String otp = request.getParameter("_otp");
//        String _status = request.getParameter("_status");
//        if (_status != null) {
//            status = Integer.parseInt(_status);
//        }
//        String result = "success";
//        String message = "request added successfully!!";
//        JSONObject json = new JSONObject();
//        PrintWriter out = response.getWriter();
//        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
//        Session sRemoteAcess = suRemoteAcess.openSession();
//        String channelName = LoadSettings.g_sSettings.getProperty("axiom.face.template");
//        if (channelName == null) {
//            channelName = "face";
//        }
//        channelName = channelName.replace(".war", "");
//        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
//        Session sChannel = suChannel.openSession();
//        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
//        Channels channel = cUtil.getChannel(channelName);
//        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
//        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
//        SessionManagement sManagement = new SessionManagement();
//        try {
//            if (credentialInfo == null) {
//                result = "error";
//                message = "Remote Access is not configured properly!!";
//            }
//            String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
//            ApEasylogin easylObj = new ApEasylogin();
//            easylObj.setIp(ip);
//            easylObj.setLattitude(lattitude);
//            easylObj.setLongitude(longitude);
//            easylObj.setUserid(userid);
//            easylObj.setChannelid(channel.getChannelid());
//            easylObj.setDeviceid(deviceid);
//            easylObj.setDeviceinfo(deviceinfo);
//            easylObj.setOtp(otp);
//            String integrity = userid + channel.getChannelid() + deviceid + otp;
//            String authsign = new String(Base64.encode(integrity.getBytes()));
//            easylObj.setAuthsign(authsign);
//            easylObj.setCreatedon(new Date());
//            EasyLoginManagement elm = new EasyLoginManagement();
//            EasyLoginSessionManagement elsm = new EasyLoginSessionManagement();
//            ApEasyloginsession EloginSession = elsm.getPendingEasyLoginSessionByUser(sessionId, channel.getChannelid(), userid);
//            if (EloginSession != null) {
//                int Rindex = -1;
//                if (EloginSession.getRequesterIndex() != null) {
//                    Rindex = EloginSession.getRequesterIndex();
//                    if (Rindex != 5) {
//                        Rindex += 1;
//                        EloginSession.setErrormsg("The Session is added!!!");
//                        EloginSession.setRequesterIndex(Rindex);
//                        EloginSession.setStatus(EasyLoginSessionManagement.ACTIVE_STATUS);
//                    } else {
//                        EloginSession.setStatus(EasyLoginSessionManagement.MAX_ATTEMPT_STATUS);
//                        EloginSession.setErrormsg("The Max attempts of the session expired!!!");
//                    }
//                } else {
//                    EloginSession.setRequesterIndex(1);
//                }
//                long current = new Date().getTime() / 1000;
//                long requestTime = EloginSession.getCreatedon().getTime() / 1000;
//                if (current - requestTime >= 300) {
//                    result = "error";
//                    message = "Request Expired!!!";
//                } else {
//                    easylObj.setStatus(ACTIVE_STATUS);
//                    Date ExpTime = new Date();
//                    ExpTime.setMinutes(ExpTime.getMinutes() + 3);
//                    easylObj.setExpireson(ExpTime);
//                    easylObj.setEasyloginId(EloginSession.getEasyloginId());
//                    result1 = elm.addEasyLoginDetails(sessionId, channel.getChannelid(), easylObj);
//                    if (result1 == 0) {
//                        elsm.editEasyLoginSessionDetails(sessionId, channel.getChannelid(), EloginSession);
//                        result = "success";
//                        String address = "";
////                        message = "Request approved successfully";
//                         if(new reverseGeoCode().GetAddress(lattitude, longitude)!=null){
//                             address =new reverseGeoCode().GetAddress(lattitude, longitude);
//                         }
//                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
//                        message = " You have Approved the Request trying from :" + EloginSession.getAppIp() + " " + address + " On " + sdf.format(EloginSession.getCreatedon());
//                    } else {
//                        result = "error";
//                        message = "Error in approving the request!!!";
//                    }
//                }
//            } else {
//                result = "error";
//                message = "No pending request!!!";
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//            }
//            out.print(callback + "(" + json + ")");
//            out.flush();
//            sRemoteAcess.close();
//            suRemoteAcess.close();
//            sChannel.close();
//            suChannel.close();
//            return;
//        }
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//
//}
