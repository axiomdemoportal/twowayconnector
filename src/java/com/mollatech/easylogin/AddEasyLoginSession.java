/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.easylogin;

import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.ApEasyloginsession;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.EasyLoginSessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
public class AddEasyLoginSession extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String callback = request.getParameter("callback");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/json");
        
        String easyloginID = request.getParameter("_easyloginID");
        String appSesssionID = request.getParameter("_appSesssionID");
        String appUserid = request.getParameter("_appUserid");
        String appChannelid = request.getParameter("_appChannelid");
        String appIP = request.getParameter("_appIP");        
              
        String status = request.getParameter("_status");
        String errormsg = request.getParameter("_errormsg");
        
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        String channelName = LoadSettings.g_sSettings.getProperty("axiom.face.template");
        if (channelName == null) {
            channelName = "face";
        }
        channelName = channelName.replace(".war", "");
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(channelName);
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        SessionManagement sManagement = new SessionManagement();
        try {
            if (credentialInfo == null) {
                String result = "error";
                String message = "Remote Access is not configured properly!!";
                json.put("_result", result);
                json.put("_message", message);

            }
            String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());

            ApEasyloginsession easylObj = new ApEasyloginsession();
                easylObj.setAppChannelid(appChannelid);
                easylObj.setAppIp(appIP);
                easylObj.setAppSesssionId(appSesssionID);
                easylObj.setAppUserid(appUserid);
                easylObj.setCreatedon(new Date());
                easylObj.setEasyloginId(easyloginID);
                easylObj.setStatus(Integer.parseInt(status));
                easylObj.setErrormsg(errormsg);
                
            int resultT = new EasyLoginSessionManagement().addEasyLoginSessionDetails(sessionId, channel.getChannelid(), easylObj);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            out.print(callback + "(" + json + ")");
            out.flush();
            sRemoteAcess.close();
            suRemoteAcess.close();
            sChannel.close();
            suChannel.close();
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
